# Maptiles component

## Application.conf settings

## Add maptiles to your PLAY application

    play.modules {
      ...
      enabled += fr.pixime.maptiles.modules.MapTilesModule
    }

### Bind maptiles cache to your play application
The following key *must be* declared in the application.conf file:

    play.cache.bindCaches = ["tiles-cache"]

### Adjust cache settings

To adjust cache settings, update the following application.conf keys:

    maptiles {
        ## The server cache duration in seconds, here 2 hours
        serverCacheDuration = 7200
        ## The client cache duration in seconds, here 30 days
        clientCacheDuration = 2592000
    }