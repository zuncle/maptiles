import com.typesafe.sbt.SbtNativePackager.autoImport.NativePackagerHelper._

name := "maptiles"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  guice,
  ehcache,
  "commons-io" % "commons-io" % "2.6",
  "org.apache.commons" % "commons-compress" % "1.5"
).map(_.exclude("org.slf4j", "slf4j-log4j12"))

mappings in (Compile, packageBin) ++= directory("modules/maptiles/public")