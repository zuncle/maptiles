package fr.pixime.maptiles.utils;

import org.apache.commons.io.FileUtils;
import play.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;
import static play.Logger.of;

/**
 * Watch file change embedded in a thread and based on WatchService
 */
public class ThreadFileWatcher extends Thread {

    private static final Logger.ALogger logger = of(ThreadFileWatcher.class);

    private WatchService watcher;
    private String folder;
    private IThreadFileWatcherFunc func;

    public ThreadFileWatcher(String folder, IThreadFileWatcherFunc func){
        this.folder = folder;
        this.func = func;
        this.setDaemon(true);
        this.setUncaughtExceptionHandler(
                (t, e) -> {
                    //logger.warn("Uncaught exception, the watched folder maybe does not exist: {}");
                    //e.printStackTrace();
                }

        );
        try {
            watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }

    public void run(){

        if ( watcher == null )
            logger.error("Unable to use watcher, run() aborted");

        File file = new File(this.folder);

	    // Create folder if needed
		if (!file.exists()){
			try {
				FileUtils.forceMkdir(file);
			}
			catch (Exception ignored){
				logger.error("Unable to create directory: " + this.folder);
			}
		}

        try {
	        file.toPath().register(watcher,
			        ENTRY_MODIFY,
			        ENTRY_CREATE,
			        ENTRY_DELETE);
        }
        catch (NoSuchFileException e) {
	        logger.error("The following folder is likely missing: " + e.getMessage());
        }
        catch (IOException e) {
            logger.error(e.getMessage());
        }

        logger.trace(file.getAbsolutePath() + " under watcher");

        for (;;) {
            // wait for key to be signaled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                logger.error("Unable to use take watcher, run() aborted");
                    Thread.yield();
                    continue;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind kind = event.kind();

                if (kind == OVERFLOW) {
                    continue;
                }

                //The filename is the context of the event.
                WatchEvent<Path> ev = cast(event);

                this.func.method(ev);
            }

            //Reset the key -- this step is critical to receive
            //further watch events. If the key is no longer valid, the directory
            //is inaccessible so exit the loop.
            boolean valid = key.reset();
            if (!valid) {
                logger.info("key not valid");
                break;
            }

            Thread.yield();
        }
    }

    public void interrupt(){
        try {
            watcher.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        super.interrupt();
    }

}
