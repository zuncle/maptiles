package fr.pixime.maptiles.utils;

import java.nio.file.Path;
import java.nio.file.WatchEvent;

/**
 * Allow using lambda expression for ThreadFileMonitor instantiation
 */
@FunctionalInterface
public interface IThreadFileWatcherFunc {
   void method(WatchEvent<Path> event);
}
