package fr.pixime.maptiles.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import play.Logger;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static play.Logger.of;

public class Zip2Shp {

	private static final Logger.ALogger logger = of(Zip2Shp.class);

	public static String generate(File zipFile) {

		String shapefileName = null;
		String zipFileFullName = zipFile.getPath();
		String zipFilePath = File.separator + FilenameUtils.getPath(zipFileFullName);

		ZipInputStream zipIn = null;
		ZipEntry entry;
		String entryName;

		boolean atLeastOneExtractedFile = false;

		logger.debug("Decompressing file {}", zipFileFullName);

		try {
			zipIn = new ZipInputStream(new FileInputStream(zipFileFullName));
			entry = zipIn.getNextEntry();

			// iterates over entries in the zip file
			while (entry != null) {
				entryName = entry.getName();
				String extension = FilenameUtils.getExtension(entryName).toLowerCase();

				if ( extension.equals("shp") || extension.equals("shx") || extension.equals("dbf")){
					if ( extractFile(zipIn, zipFilePath + entryName) ) {
						logger.trace("{} decompressed", entryName);
						if ( extension.equals("shp") )
							shapefileName = entryName;
					}
				}

				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
			}

			// delete zip file
			try { FileUtils.forceDelete(zipFile); } catch (Exception ignored) {}

			return shapefileName;

		} catch (IOException e) {
			logger.error(e.getMessage());
		}

		try {
			if (zipIn != null) {
				zipIn.close();
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	private static boolean extractFile(ZipInputStream zipIn, String filePath) throws IOException {
		int BUFFER_SIZE = 4096;

		logger.trace("Extracting {}", filePath);

		try {
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
			//BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("./test.bin"));
			byte[] bytesIn = new byte[BUFFER_SIZE];
			int read = 0;
			while ((read = zipIn.read(bytesIn)) != -1) {
				bos.write(bytesIn, 0, read);
			}
			bos.close();
			return true;
		}
		catch(Exception e){
			logger.trace(e.getMessage());
			return false;
		}
	}
}
