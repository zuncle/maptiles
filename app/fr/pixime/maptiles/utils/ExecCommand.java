package fr.pixime.maptiles.utils;

import fr.pixime.maptiles.actors.MapTilesProcess;
import play.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;


import static play.Logger.of;

public class ExecCommand {

	private static final Logger.ALogger logger = of(ExecCommand.class);

	private Semaphore outputSem;
	private String output;
	private Semaphore errorSem;
	private String error;
	private Process p;
	private int exitValue;

	/*
	private class InputWriter extends Thread {
		private final String input;

		InputWriter(String input) {
			this.input = input;
		}

		public void run() {
			PrintWriter pw = new PrintWriter(p.getOutputStream());
			pw.println(input);
			pw.flush();
		}
	}
	*/

	private class OutputReader extends Thread {
		private final MapTilesProcess process;
		private final IThreadExecCommandFunc onOutput;

		OutputReader() {
			this(null, null);
		}

		OutputReader(MapTilesProcess process, IThreadExecCommandFunc onOutput) {
			this.process = process;
			this.onOutput = onOutput;
			try {
				outputSem = new Semaphore(1);
				outputSem.acquire();
			} catch (InterruptedException e) {
				logger.warn(e.getMessage());
			}
		}

		public void run() {
			try {
				StringBuilder readBuffer = new StringBuilder();
				BufferedReader isr = new BufferedReader(new InputStreamReader(p
						.getInputStream()));
				String buff;
				while ((buff = isr.readLine()) != null) {
					readBuffer.append(buff);
					//System.out.println(buff);
					logger.warn(buff);
					if ( this.onOutput != null && this.process != null )
						this.onOutput.method(process, buff);
				}
				output = readBuffer.toString();
				outputSem.release();
			} catch (IOException e) {
				logger.warn(e.getMessage());
			}
		}
	}

	private class ErrorReader extends Thread {
		private final MapTilesProcess process;
		private final IThreadExecCommandFunc onError;

		ErrorReader(){
			this(null, null);
		}

		ErrorReader(MapTilesProcess process, IThreadExecCommandFunc onError) {
			this.process = process;
			this.onError = onError;
			try {
				errorSem = new Semaphore(1);
				errorSem.acquire();
			} catch (InterruptedException e) {
				logger.warn(e.getMessage());
			}
		}

		public void run() {
			try {
				StringBuilder readBuffer = new StringBuilder();
				BufferedReader isr = new BufferedReader(new InputStreamReader(p
						.getErrorStream()));
				String buff;
				while ((buff = isr.readLine()) != null) {
					readBuffer.append(buff);
					if ( this.onError != null && this.process != null )
						this.onError.method(process, buff);
				}
				error = readBuffer.toString();
				errorSem.release();
			} catch (IOException e) {
				logger.warn(e.getMessage());
			}
		}
	}

	public ExecCommand(String command,
				MapTilesProcess process) throws IOException, InterruptedException {
		logger.debug("Executing \"{}\"", command);
		p = Runtime.getRuntime().exec(makeArray(command));
		new OutputReader(process, process.getOnOutput()).start();
		new ErrorReader(process, process.getOnError()).start();
		p.waitFor();
		exitValue = p.exitValue();
	}

	public int getExitValue() {
		return exitValue;
	}

	public String getOutput() {
		try {
			outputSem.acquire();
		} catch (InterruptedException e) {
			logger.warn(e.getMessage());
		}
		String value = output;
		outputSem.release();
		return value;
	}

	public String getError() {
		try {
			errorSem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String value = error;
		errorSem.release();
		return value;
	}

	private String[] makeArray(String command) {
		ArrayList<String> commandArray = new ArrayList<>();
		StringBuilder buff = new StringBuilder();
		boolean lookForEnd = false;
		for (int i = 0; i < command.length(); i++) {
			if (lookForEnd) {
				if (command.charAt(i) == '\"') {
					if (buff.length() > 0)
						commandArray.add(buff.toString());
					buff = new StringBuilder();
					lookForEnd = false;
				} else {
					buff.append(command.charAt(i));
				}
			} else {
				if (command.charAt(i) == '\"') {
					lookForEnd = true;
				} else if (command.charAt(i) == ' ') {
					if (buff.length() > 0)
						commandArray.add(buff.toString());
					buff = new StringBuilder();
				} else {
					buff.append(command.charAt(i));
				}
			}
		}
		if (buff.length() > 0)
			commandArray.add(buff.toString());

		String[] array = new String[commandArray.size()];
		for (int i = 0; i < commandArray.size(); i++) {
			array[i] = commandArray.get(i);
		}

		return array;
	}
}
