package fr.pixime.maptiles.utils;


import fr.pixime.maptiles.actors.MapTilesProcess;

/**
 * Allow using lambda expression for ThreadFileMonitor instantiation
 */
@FunctionalInterface
public interface IThreadExecCommandFunc {
   void method(MapTilesProcess process, String buffer);
}
