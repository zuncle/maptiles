package fr.pixime.maptiles.utils;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.FileUtils;
import play.Logger;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static play.Logger.of;

/**
 * (compressed) Pbf -> pbf.uncompressed.gz -> (decompressed in) pbf.uncompressed
 */
public class UncompressedPbf {
	private static final Logger.ALogger logger = of(UncompressedPbf.class);

	private File pbfFile;

	public UncompressedPbf(@NotNull File pbfFile){
		this.pbfFile = pbfFile;
	}

	public boolean run() throws IOException, InterruptedException {


		String filename = this.pbfFile.getAbsolutePath();

		long start = System.currentTimeMillis();

		if ( filename.endsWith(".pbf") ) {
			// rename pbf file to pbf.uncompressed.gz
			String uncompressedFilename = String.format("%s.uncompressed.gz", filename);
			File uncompressedFile = new File(uncompressedFilename);
			FileUtils.copyFile(pbfFile, uncompressedFile);

			if ( ! uncompressedFile.exists() ){
				logger.error("Unable to copy {} to {}", filename, uncompressedFilename);
				return false;
			}

			InputStream fin = Files.newInputStream(Paths.get(uncompressedFilename));
			BufferedInputStream in = new BufferedInputStream(fin);
			OutputStream out = Files.newOutputStream(Paths.get(String.format("%s.uncompressed", filename)));
			GzipCompressorInputStream gzIn = new GzipCompressorInputStream(in);

			final byte[] buffer = new byte[8182];
			int n = 0;
			while (-1 != (n = gzIn.read(buffer))) {
				out.write(buffer, 0, n);
			}

			FileUtils.forceDelete(uncompressedFile);

			out.close();
			gzIn.close();

			logger.trace("Pbf {} uncompressed in {} ms", filename, System.currentTimeMillis() - start);
			return true;
		}
		return false;
	}
}
