package fr.pixime.maptiles.utils.Shp2Json;

import fr.pixime.maptiles.actors.MapTilesProcess;
import fr.pixime.maptiles.utils.ExecCommand;
import play.Logger;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;

import static play.Logger.of;

/**
 * Convert shapefile into geo json, ex:
 * ogr2ogr -f GeoJSON xxx.json -t_srs EPSG:4326 xxx.shp
 */
class Ogr2ogr {
    private static final Logger.ALogger logger = of(Ogr2ogr.class);

    private final File shapeFile;
    private final File jsonFile;
    private final boolean noPrj;

    Ogr2ogr(@NotNull File shapeFile, @NotNull File jsonFile, boolean noPrj){
        this.shapeFile = shapeFile;
        this.jsonFile = jsonFile;
        this.noPrj = noPrj;
    }

    public boolean run(MapTilesProcess process) throws IOException, InterruptedException {

        //String cmd = "ogr2ogr -f GeoJSON %s -t_srs EPSG:4326 %s";
        // if no projection, we assume shapefile uses WGS84
        //if ( noPrj )
        //    cmd = "ogr2ogr -f GeoJSON %s -t_srs EPSG:4326 s_srs EPSG:4326 %s";

        String cmd = "ogr2ogr -f GeoJSON -preserve_fid %s %s";
        // Assumption: GDAL is available from $PATH
        cmd = String.format(cmd,
                this.jsonFile.getAbsolutePath(),
                this.shapeFile.getAbsolutePath());

        ExecCommand execCommand = new ExecCommand(cmd, process);
        if (execCommand.getExitValue() == 0){
            logger.trace(execCommand.getOutput());
            return true;
        }
        else {
            logger.trace(execCommand.getError());
            return false;
        }
    }
}
