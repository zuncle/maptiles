package fr.pixime.maptiles.utils.Shp2Json;

import fr.pixime.maptiles.actors.MapTilesProcess;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import play.Logger;

import java.io.File;
import java.io.IOException;

import static play.Logger.of;

public class Shp2Json {

	private static final Logger.ALogger logger = of(Shp2Json.class);

	private final String filenameWithoutExtension;
	private final String folder;

	/**
	 * ShapeFilename can be have extension .SHX, .PRJ, .DBF or .SHP
	 * @param shapeFilename
	 */
	public Shp2Json(String shapeFilename){
		this.folder = FilenameUtils.getFullPath(shapeFilename);
		this.filenameWithoutExtension = FilenameUtils.getBaseName(shapeFilename);
	}

	public MapTilesProcess.Status generate(MapTilesProcess process){

		long start = System.currentTimeMillis();

		logger.trace("Converting shapefile {} to json file", filenameWithoutExtension);

		if ( ! checkShapefiles(folder, filenameWithoutExtension) ){
			logger.trace("one of shx, dbf or shp file {} is missing in folder", filenameWithoutExtension, folder);
			return MapTilesProcess.Status.CANCELLED;
		}

		// set shp file name
		String shpFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "shp");
		// set shx file name
		String shxFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "shx");
		// set dbf file name
		String dbfFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "dbf");
		// set prj file name
		String prjFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "prj");
		// set json file name
		String jsonFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "json");
		// set mbtiles file name
		//String mbtilesFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "mbtiles");

		// delete json file if exists
		File jsonFile = new File(jsonFilename);
		if ( jsonFile.exists() ){
			try {
				FileUtils.forceDelete(jsonFile);
			} catch (IOException e) {
				logger.error("Unable to delete existing json file {}", jsonFilename);
			}
		}

		File shpFile = new File(shpFilename);

		// check if prj file exists
		boolean noPrj = true;
		File prjFile = new File(prjFilename);
		if ( prjFile.exists() ){
			noPrj = false;
		}

		Ogr2ogr ogr2ogr = new Ogr2ogr(shpFile, jsonFile, noPrj);
		try {
			boolean success = ogr2ogr.run(process);
			if ( success ) {
				logger.info("Shapefile {} succesfully converted to {} in {} ms",
						shpFilename, jsonFilename, System.currentTimeMillis() -start);
			}
			else {
				logger.error("ogr2ogr has failed using {}", shpFilename);
				return MapTilesProcess.Status.FAILED;
			}
		} catch (IOException | InterruptedException e) {
			logger.error("Unable to process ogr2ogr {}: {}", shpFilename, e.getMessage());
			return MapTilesProcess.Status.FAILED;
		}

		// delete input and intermediate files
		try { FileUtils.forceDelete(new File(shpFilename)); } catch (Exception ignored) {}
		try { FileUtils.forceDelete(new File(dbfFilename)); } catch (Exception ignored) {}
		try { FileUtils.forceDelete(new File(shxFilename)); } catch (Exception ignored) {}
		try { FileUtils.forceDelete(new File(prjFilename)); } catch (Exception ignored) {}
		logger.trace("Files shp, shx, dbf, prj removed");

		return MapTilesProcess.Status.SUCCESS;
	}

	/**
	 * Check if minimal shapefile files are present in a given folder: shx, dbf, shp
	 * @param folder Where the file is located
	 * @param fileWithoutExtension Simple file name without extension
	 * @return true if shp, dbf and shx are present, otherwise false
	 */
	public static boolean checkShapefiles(String folder, String fileWithoutExtension){

		String dbfFilename = String.format("%s%s.%s", folder, fileWithoutExtension, "dbf");
		File dbfFile = new File(dbfFilename);
		if ( !dbfFile.exists() ){
			logger.trace("dbf file {} does not exist", dbfFilename);
			return false;
		}

		String shxFilename = String.format("%s%s.%s", folder, fileWithoutExtension, "shx");
		File shxFile = new File(shxFilename);
		if ( !shxFile.exists() ){
			logger.trace("shx file {} does not exist", shxFilename);
			return false;
		}

		String shpFilename = String.format("%s%s.%s", folder, fileWithoutExtension, "shp");
		File shpFile = new File(shpFilename);
		if ( !shpFile.exists() ){
			logger.trace("shp file {} does not exist", shpFilename);
			return false;
		}

		return true;
	}
}
