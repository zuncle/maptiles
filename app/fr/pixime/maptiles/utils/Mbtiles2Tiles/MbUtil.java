package fr.pixime.maptiles.utils.Mbtiles2Tiles;

import fr.pixime.maptiles.actors.MapTilesProcess;
import fr.pixime.maptiles.utils.ExecCommand;

import javax.validation.constraints.NotNull;
import java.io.*;

/**
 * Create a tiles directory from .mbtiles file
 */
class MbUtil {

    public enum ImageFormat {
        PBF,
        PNG
    }

    private File mbTilesFile;
    private File outputDirectory;
    private String formatImage = "pbf";

    MbUtil(@NotNull File mbTilesFile, @NotNull File outputDirectory, MbUtil.ImageFormat format){
        this.mbTilesFile = mbTilesFile;
        this.outputDirectory = outputDirectory;
        switch (format) {
            case PBF:
                this.formatImage = "pbf";
                break;
            case PNG:
                this.formatImage = "png";
                break;
        }
    }

    public boolean run(MapTilesProcess process) throws IOException, InterruptedException {
    	// Assumption: mb-util is available from $PATH, e.g. export PATH=$PATH:/usr/local/mbutil
        String cmd = String.format("mb-util --image_format=%s %s %s",
                this.formatImage,
                mbTilesFile.getAbsolutePath(),
                this.outputDirectory.getAbsolutePath());

        ExecCommand execCommand = new ExecCommand(cmd, process);
		return (execCommand.getExitValue() == 0);
    }
}
