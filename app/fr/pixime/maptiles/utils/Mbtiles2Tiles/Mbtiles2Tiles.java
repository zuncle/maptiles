package fr.pixime.maptiles.utils.Mbtiles2Tiles;

import fr.pixime.maptiles.actors.MapTilesProcess;
import org.apache.commons.io.FileUtils;
import play.Logger;

import java.io.File;
import java.io.IOException;

import static play.Logger.of;

public class Mbtiles2Tiles {

	private static final Logger.ALogger logger = of(Mbtiles2Tiles.class);

	private String filename;
	private String foldername;

	public Mbtiles2Tiles(String filename, String foldername){
		this.filename = filename;
		this.foldername = foldername;
	}

	public MapTilesProcess.Status generate(MapTilesProcess process){

		long start = System.currentTimeMillis();

		logger.trace("Generating PBF tiles from {} to {}", filename, foldername);

		File file = new File(filename);
		if ( !file.exists() ){
			logger.error("File {} does not exist", filename);
			return MapTilesProcess.Status.CANCELLED;
		}

		File folder = new File(foldername);
		if ( folder.exists() ){
			try {
				FileUtils.deleteDirectory(folder);
			} catch (IOException e) {
				logger.error("Unable to delete folder {}", foldername);
			}
		}

		MbUtil mbUtils = new MbUtil(file, folder, MbUtil.ImageFormat.PBF);
		try {
			boolean success = mbUtils.run(process);
			if ( success ) {
				logger.info("PBF tiles succesfully generated in directory {} in {} ms",
						foldername, System.currentTimeMillis() - start);

				// Delete mbtiles file
				try { FileUtils.forceDelete(new File(process.getFile())); } catch (Exception ignored) {}

				return MapTilesProcess.Status.SUCCESS;
			}
			else {
				logger.error("Tiles generation has failed using {}", filename);
				return MapTilesProcess.Status.FAILED;
			}

		} catch (IOException | InterruptedException e) {
			logger.error("Unable to process mb-util {}: {}", foldername, e.getMessage());
		}
		return MapTilesProcess.Status.FAILED;
	}
}
