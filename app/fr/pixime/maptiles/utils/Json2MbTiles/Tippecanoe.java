package fr.pixime.maptiles.utils.Json2MbTiles;

import fr.pixime.maptiles.actors.MapTilesProcess;
import fr.pixime.maptiles.utils.ExecCommand;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;

/**
 * Convert shapefile into geo json, ex:
 * tippecanoe -o xxx.mbtiles -zg --drop-densest-as-needed xxx.json
 */
class Tippecanoe {

    private File mbtilesFile;
    private File jsonFile;

    Tippecanoe(@NotNull File jsonFile, @NotNull File mbtilesFile){
        this.mbtilesFile = mbtilesFile;
        this.jsonFile = jsonFile;
    }

    public boolean run(MapTilesProcess process) throws IOException, InterruptedException {

    	// Assumption: tippecanoe is available from $PATH,
        String cmd = String.format("tippecanoe -o %s -f -z14 --drop-densest-as-needed --generate-ids %s",
        //String cmd = String.format("tippecanoe -o %s -f -z16 --drop-densest-as-needed %s",
            this.mbtilesFile.getAbsolutePath(),
            this.jsonFile.getAbsolutePath());

        ExecCommand execCommand = new ExecCommand(cmd, process);
		return (execCommand.getExitValue() == 0);
    }
}
