package fr.pixime.maptiles.utils.Json2MbTiles;

import fr.pixime.maptiles.actors.MapTilesProcess;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import play.Logger;

import java.io.File;
import java.io.IOException;

import static play.Logger.of;

public class Json2MbTiles {

	private static final Logger.ALogger logger = of(Json2MbTiles.class);

	private final String jsonFilename;
	private final String filenameWithoutExtension;
	private final String folder;

	public Json2MbTiles(String jsonFilename){
		this.jsonFilename = jsonFilename;
		this.folder = FilenameUtils.getFullPath(jsonFilename);
		this.filenameWithoutExtension = FilenameUtils.getBaseName(jsonFilename);
	}

	public MapTilesProcess.Status generate(MapTilesProcess process){

		long start = System.currentTimeMillis();

		logger.trace("Converting json {} to mbtiles file", filenameWithoutExtension);

		// set mbtiles file name
		String mbtilesFilename = String.format("%s%s.%s", folder, filenameWithoutExtension, "mbtiles");


		// check json file if exists
		File jsonFile = new File(jsonFilename);
		if ( ! jsonFile.exists() ){
			logger.error("{} does not exists", this.jsonFilename);
			return MapTilesProcess.Status.CANCELLED;
		}

		// delete mbtiles file if exists
		File mbtilesFile = new File(mbtilesFilename);
		if ( mbtilesFile.exists() ){
			try {
				FileUtils.forceDelete(mbtilesFile);
			} catch (IOException e) {
				logger.error("Unable to delete existing mbtiles {}", mbtilesFilename);
			}
		}

		MapTilesProcess.Status status = MapTilesProcess.Status.CANCELLED;
		Tippecanoe tippecanoe = new Tippecanoe(jsonFile, mbtilesFile);
		try {
			boolean success = tippecanoe.run(process);
			if ( success ) {
				logger.info("Json file {} succesfully converted to {} in {} ms",
						jsonFilename, mbtilesFilename, System.currentTimeMillis() -start);
				status = MapTilesProcess.Status.SUCCESS;
			}
			else {
				logger.error("Tippecanoe has failed using {}", jsonFilename);
				try { FileUtils.forceDelete(mbtilesFile); } catch (Exception ignored) {}
				status = MapTilesProcess.Status.FAILED;
			}
		} catch (IOException | InterruptedException e) {
			try { FileUtils.forceDelete(mbtilesFile); } catch (Exception ignored) {}
			logger.error("Unable to process tippecanoe {}: {}", jsonFilename, e.getMessage());
			status = MapTilesProcess.Status.FAILED;
		}

		// delete json file
		try { FileUtils.forceDelete(jsonFile); } catch (Exception ignored) {}

		return status;
	}
}
