package fr.pixime.maptiles.models;

import java.io.File;

public class BuiltInPaths {
	public static final String FOLDER_ROOT          = "data/maptiles" + File.separator;
	public static final String FOLDER_TILES_NAME    = "tiles";
	public static final String FOLDER_STYLES_NAME   = "styles";

	public static final String FOLDER_TILES         = FOLDER_ROOT + FOLDER_TILES_NAME + File.separator;     // data/maptiles/tiles
	public static final String FOLDER_STYLES		= FOLDER_ROOT + FOLDER_STYLES_NAME + File.separator;	// data/maptiles/styles
}



