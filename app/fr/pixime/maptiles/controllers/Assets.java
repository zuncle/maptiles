package fr.pixime.maptiles.controllers;

import play.api.mvc.Action;
import play.api.mvc.AnyContent;

import javax.inject.Inject;

/**
 * @author Pixime on 01/03/2017
 */
public class Assets {
    @Inject
    controllers.Assets assets;

    /*
    public Action<AnyContent> at(String file) {
    	return assets.at("/public/lib/maptiles", file, false);
    }
     */

	public Action<AnyContent> getFont(String font) {
    	return assets.at("/public/lib/maptiles/fonts", font, true);
	}

	public Action<AnyContent> getSprites(String font) {
		return assets.at("/public/lib/maptiles/sprites", font, true);
	}
}


