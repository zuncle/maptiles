package fr.pixime.maptiles.controllers.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.pixime.maptiles.actors.MapTilesProcess;
import org.apache.commons.io.FilenameUtils;

import java.time.ZoneOffset;

public class ProcessResponse {
	private final MapTilesProcess process;

	public ProcessResponse(MapTilesProcess process) {
		this.process = process;
	}

	public String getId() {
		return process.getId();
	}

	public String getFile() {
		return FilenameUtils.getBaseName(process.getFile());
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getMessage() {
		return process.getMessage();
	}

	public MapTilesProcess.Status getStatus() {
		return process.getStatus();
	}

	public Long getQueuedDate() {
		return process.getQueuedDate().atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Long getRunDate() {
		if ( process.getRunDate() != null )
			return process.getRunDate().atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
		else
			return null;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Long getEndDate() {
		if ( process.getEndDate() != null )
			return process.getEndDate().atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
		else
			return null;
	}
}
