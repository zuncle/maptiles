package fr.pixime.maptiles.controllers.responses;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class ProcessesResponse {
	private final long issueDate;
	private final List<ProcessResponse> processes;

	public ProcessesResponse(List<ProcessResponse> processes) {
		this.issueDate = LocalDateTime.now().atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
		this.processes = processes;
	}

	public long getIssueDate() {
		return issueDate;
	}

	public List<ProcessResponse> getProcesses() {
		return processes;
	}
}
