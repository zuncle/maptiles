package fr.pixime.maptiles.controllers;

import akka.actor.ActorRef;
import fr.pixime.maptiles.actors.MapTilesProtocol;
import fr.pixime.maptiles.controllers.responses.ProcessResponse;
import fr.pixime.maptiles.controllers.responses.ProcessesResponse;
import fr.pixime.maptiles.models.BuiltInPaths;
import play.Logger;
import play.mvc.Http.Request;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import javax.inject.Named;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static akka.pattern.Patterns.ask;
import static play.Logger.of;

public class ApplicationController extends Controller {

	private static final Logger.ALogger logger = of(ApplicationController.class);

	private static final int TIMEOUT = 60000; // milliseconds

	private final ActorRef mapTilesActor;

	@Inject
	public ApplicationController(@Named("map-tiles-actor") ActorRef mapTilesActor) {
		this.mapTilesActor = mapTilesActor;
	}

	public CompletionStage<Result> getProcesses() {
		return FutureConverters.toJava(
				ask(this.mapTilesActor,
					new MapTilesProtocol.ProcessesRequest(),
					TIMEOUT))
				.thenApply((response) -> {
					MapTilesProtocol.ProcessesResponse processesResponse =
							(MapTilesProtocol.ProcessesResponse)response;
					return ok (Json.toJson(
							new ProcessesResponse(
									processesResponse.getProcesses()
											.stream()
											.map(ProcessResponse::new)
											.collect(Collectors.toList())
							)));
				});
	}

	public CompletionStage<Result> getTilesFolders() {
		return FutureConverters.toJava(
				ask(this.mapTilesActor,
						new MapTilesProtocol.TilesFoldersRequest(),
						TIMEOUT))
				.thenApply((response) -> {
					MapTilesProtocol.TilesFoldersResponse tilesFoldersResponse =
							(MapTilesProtocol.TilesFoldersResponse)response;
					return ok (Json.toJson(tilesFoldersResponse.getTilesFolders()));
				});
	}

	public Result getStyle(String style) {
		String jsonStyleFilename = String.format("%s.json", style);
		File jsonStyleFile = new File(String.format("%s%s", BuiltInPaths.FOLDER_STYLES, jsonStyleFilename));
		if (!jsonStyleFile.exists()) {
			return notFound(String.format("Theme %s not found", style));
		}
		try {
			String contents = new String(Files.readAllBytes(Paths.get(jsonStyleFile.getAbsolutePath())));
			return ok(contents);
		} catch (IOException e) {
			return internalServerError("Can't read style file");
		}

	}

	public Result getStyles(String style, String tiles, Request request){
		String jsonStyleFilename = String.format("%s.json", style);
		File jsonStyleFile = new File(String.format("%s%s", BuiltInPaths.FOLDER_STYLES, jsonStyleFilename));
		if ( !jsonStyleFile.exists() ){
			return notFound(String.format("Theme %s not found", style));
		}

		String folderName = String.format("%s%s", fr.pixime.maptiles.models.BuiltInPaths.FOLDER_TILES, tiles);
		File folder = new File(folderName);
		if ( !folder.exists() || !folder.isDirectory() ){
			return notFound(String.format("Tiles %s not found", tiles));
		}

		try {
			String contents = new String(Files.readAllBytes(Paths.get(jsonStyleFile.getAbsolutePath())))
					.replace("$host$", String.format("http://%s", request.host()))
					.replace( "$hosts$", String.format("https://%s", request.host()))
					.replace("$tiles$", tiles);
			return ok(contents);
		} catch (IOException e) {
			return internalServerError("Can't process style file");
		}
	}
}
