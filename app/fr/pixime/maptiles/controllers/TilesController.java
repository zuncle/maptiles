package fr.pixime.maptiles.controllers;

import akka.Done;
import com.typesafe.config.Config;
import fr.pixime.maptiles.models.BuiltInPaths;
import fr.pixime.maptiles.utils.UncompressedPbf;
import org.apache.commons.io.FileUtils;
import play.Logger;
import play.cache.AsyncCacheApi;
import play.cache.NamedCache;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.Logger.of;

public class TilesController extends Controller {

	private static final Logger.ALogger logger = of(TilesController.class);

	private int serverCacheDuration = 60 * 60 * 2;        // server cache duration : 2 hours
	private int clientCacheDuration = 60 * 60 * 24 * 30;  // client cache duration : 30 days

	@Inject
	Config config;

	@Inject
	@NamedCache("tiles-cache")
	AsyncCacheApi cache;

	@Inject
	HttpExecutionContext httpExecutionContext;

	public TilesController() {
		try {
			serverCacheDuration = config.getInt("maptiles.serverCacheDuration");
		}
		catch (Exception ignored){
		}

		try {
			clientCacheDuration = config.getInt("maptiles.clientCacheDuration");
		}
		catch (Exception ignored){
		}
	}

	/**
	 * Return a pbf file (tile) using the z/x/y notation
	 * @param folder where the tiles can be found (this folder must be placed below /data/mapgl/tiles/)
	 * @param z z tile
	 * @param x x tile
	 * @param y y tile
	 * @param clientCache Enable client caching (useful for background layers like OSM, do not be used for frequently updated layers)
	 * @param serverCache Enable server caching (useful for background layers like OSM, do not be used for frequently updated layers)
	 */
	public CompletionStage<Result> getTiles(String folder,
	                                        Integer z,
	                                        Integer x,
	                                        Integer y,
	                                        Boolean clientCache,
	                                        Boolean serverCache) {

		long start = System.currentTimeMillis();

		String cacheKey = String.format("%s.%d.%d.%d", folder, z, x, y);

		CompletionStage<byte[]> tile;
		if ( serverCache ) {
			tile = cache.getOrElseUpdate(cacheKey, (() -> this.getTile(folder, z, x, y, serverCache, cacheKey)));
		}
		else {
			tile = getTile(folder, z, x, y, false, null);
		}

		return tile.thenApply(content -> {
			if ( content == null )
				return noContent();

			logger.trace("Tile {}/{}/{}/{} loaded in {} ms", folder, z, x, y, System.currentTimeMillis()-start);

			Result result = ok(content);
			if ( !clientCache )
				return result;
			else
				return result.withHeader(CACHE_CONTROL, String.format("max-age=%d", clientCacheDuration));
		});
	}

	/**
	 * Retrieve a pbf tile from a tiles folder. The tile is decompressed if required
	 * and added to the PLAY cache if server cache is enabled
	 * @param folder The search folder of the tile (e.g /files/maptiles/tiles/osm)
	 * @param z z tiles
	 * @param x x tile
	 * @param y y tile
	 * @param serverCache True if server cache is enabled, otherwise false
	 * @param cacheKey The name of the cache key
	 * @return The content of the pbf file if found, null if something goes wrong
	 */
	private CompletionStage<byte[]> getTile(String folder,
	                                        Integer z, Integer x, Integer y,
	                                        boolean serverCache,
	                                        String cacheKey){

		String tilePath = String.format("%s%s/%d/%d/%d", BuiltInPaths.FOLDER_TILES, folder, z, x, y);

		return CompletableFuture.supplyAsync( () -> {
			File tileFile = this.findOrCreateUncompressedTile(tilePath);

			if (tileFile == null) {
				//Important, the tileset may not have the requested tile, in this case the HTTP result must be 200 !
				return null;
			}

			if (tileFile.exists()) {
				try {
					byte[] bytes;
					bytes = FileUtils.readFileToByteArray(tileFile);

					if ( serverCache ) {
						logger.trace("store tile {}/{}/{}/{} in cache", folder, z, x, y);
						// TODO cache delay... ?
						CompletionStage<Done> result = cache.set(cacheKey, bytes, serverCacheDuration);
					}
					return bytes;
				} catch (Exception ignored) {
					return null;
				}
			}
			return null;
		}, httpExecutionContext.current());
	}

	/**
	 * If the tile is a PNG, return the corresponding File, otherwise
	 * return an uncompressed pbf file if exists, otherwise decompress the pbf file and return the result.
	 * uncompressed file are renamed using the pbf filename followed by "uncompressed",
	 * e.g 8769.pbf -> 8768.pbf.uncompressed
	 * @param tilePath the tile path without extension, e.g "/maptiles/tiles/osm/0/0/1" for "1.png" or "1.pbf"
	 * @return a png file or an uncompressed pbf file, e.g 8768.png or 8768.pbf.uncompressed
	 */
	private File findOrCreateUncompressedTile(String tilePath){

		// is it PNG ?
		String pngFilename = String.format("%s.%s", tilePath, "png");
		File pngFile = new File(pngFilename);
		if ( pngFile.exists() )
			return pngFile;

		// is it PBF ?
		String pbfFilename = String.format("%s.%s", tilePath, "pbf");
		// An uncompressed version of the file exists ?
		File uncompressedPbfFile = new File(pbfFilename + ".uncompressed");
		if ( uncompressedPbfFile.exists() ){
			return uncompressedPbfFile;
		}

		// uncompressed file does not exists
		File pbfFile = new File(pbfFilename);
		// pbf file does not exists
		if ( ! pbfFile.exists() ){
			// !important
			return null;
		}

		// pbf file exists, let's uncompress
		try {
			UncompressedPbf uncompressPbf = new UncompressedPbf(pbfFile);
			if ( ! uncompressPbf.run() ) {
				return null;
			}
		} catch (IOException | InterruptedException e) {
			return null;
		}
		uncompressedPbfFile = new File(pbfFilename + ".uncompressed");
		if ( !uncompressedPbfFile.exists() ){
			return null;
		}

		return uncompressedPbfFile;
	}

}
