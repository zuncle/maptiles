package fr.pixime.maptiles.modules;

import com.google.inject.AbstractModule;
import fr.pixime.maptiles.actors.MapTilesActor;
import fr.pixime.maptiles.actors.ProcessorActor;
import play.Logger;
import play.libs.akka.AkkaGuiceSupport;

import static play.Logger.of;

public class MapTilesModule extends AbstractModule implements AkkaGuiceSupport{

    private static final Logger.ALogger logger = of(MapTilesModule.class);

    @Override
    public void configure() {
	    logger.info("[maptiles] Starting module...");

	    bindActor(ProcessorActor.class, "processor-actor");
	    bindActor(MapTilesActor.class, "map-tiles-actor");
        bind(OnStartup.class).asEagerSingleton();

	    logger.info("[maptiles] Module started");
    }
}
