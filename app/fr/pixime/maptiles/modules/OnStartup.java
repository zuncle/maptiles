package fr.pixime.maptiles.modules;

import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

import static play.Logger.of;

@Singleton
public class OnStartup {

    private static final Logger.ALogger logger = of(OnStartup.class);

    @Inject
    public OnStartup() {
        logger.info("[mapTiles] Initializing module...");

	    logger.info("[mapTiles] Module initialized");
    }
}
