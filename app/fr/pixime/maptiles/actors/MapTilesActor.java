package fr.pixime.maptiles.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import fr.pixime.maptiles.models.BuiltInPaths;
import fr.pixime.maptiles.utils.Shp2Json.Shp2Json;
import fr.pixime.maptiles.utils.ThreadFileWatcher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static play.Logger.of;

public class MapTilesActor extends AbstractActor {

	private static final Logger.ALogger logger = of(MapTilesActor.class);

	List<MapTilesProcess> processes = new ArrayList<>();
	List<String> tilesFolders = new ArrayList<>();

	private ThreadFileWatcher watcher;

	@Inject
	Config config;

	@Inject
	private @Named("processor-actor")
	ActorRef processorActor;

	@Inject
	public MapTilesActor() {
		logger.debug("[MapTilesActor] constructor");
	}

	@Override
	public void preStart() throws Exception {
		logger.info("[MapTilesActor] preStart");

		this.ensureTilesFolderExists();
		this.ensureStylesFolderExists();
		//The list of tiles folders is now updated on user request
		//this.updateTilesFolders();
		//

		boolean onlyServer = false;
		try {
			onlyServer = config.getBoolean("maptiles.onlyServer");
		}
		catch (ConfigException.Missing e){
			logger.info("maptiles.onlyServer configuration entry is missing, false is assumed");
		}
		catch (ConfigException.WrongType e){
			logger.error("Wrong type for maptiles.onlyServer configuration entry, false is assumed");
		}

		if ( ! onlyServer ) {
			this.watcher = new ThreadFileWatcher(
					BuiltInPaths.FOLDER_ROOT,
					this::onFileChange);
			this.watcher.start();
		}
		else {
			logger.info("Maptiles is only a tiles server");
		}
		super.preStart();
	}

	@Override
	public void postStop() throws Exception {
		logger.info("[MapTilesActor] postStop");
		watcher.interrupt();
		super.postStop();
	}

	private void ensureTilesFolderExists(){
		// Create TILES folder if does not exists
		File tilesFolder = new File(BuiltInPaths.FOLDER_TILES);
		if ( !tilesFolder.exists() ) {
			// create folder tiles if needed
			try {
				FileUtils.forceMkdir(tilesFolder);
			} catch (Exception e) {
				logger.error("Unable to create folder: " + BuiltInPaths.FOLDER_TILES);
			}
		}
	}

	private void ensureStylesFolderExists(){
		// Create TILES folder if does not exists
		File tilesFolder = new File(BuiltInPaths.FOLDER_STYLES);
		if ( !tilesFolder.exists() ) {
			// create folder tiles if needed
			try {
				FileUtils.forceMkdir(tilesFolder);
			} catch (Exception e) {
				logger.error("Unable to create folder: " + BuiltInPaths.FOLDER_STYLES);
			}
		}
	}

	/**
	 * Executed each time a map configuration file change (files located in BuiltInPaths.FOLDER_ROOT folder)
	 */
	private void onFileChange(WatchEvent<Path> event) {
		Path filepath = event.context();

		String currentPath = new File("").getAbsolutePath();
		String filename = String.format("%s%s%s%s", currentPath, File.separator, BuiltInPaths.FOLDER_ROOT, filepath);

		File file = new File(filename);

		String extension = FilenameUtils.getExtension(file.getName()).toLowerCase();

		if (extension.equalsIgnoreCase("zip")){
			this.getSelf().tell(new MapTilesProtocol.HandleFileChange(
					file, event.kind().name(), MapTilesProtocol.FileType.ZIP), ActorRef.noSender());
		}

		if (extension.equalsIgnoreCase("json")){
			this.getSelf().tell(new MapTilesProtocol.HandleFileChange(
					file, event.kind().name(), MapTilesProtocol.FileType.JSON), ActorRef.noSender());
		}

		else if (extension.equalsIgnoreCase("mbtiles"))
			this.getSelf().tell(new MapTilesProtocol.HandleFileChange(
					file, event.kind().name(), MapTilesProtocol.FileType.MBTILES), ActorRef.noSender());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(MapTilesProtocol.HandleFileChange.class, message ->
						this.handleFileChange(message.getFile(), message.getEventName(), message.getFileType()))
				.match(MapTilesProtocol.RunResponse.class, message ->
						this.handleRunResponse(message.getProcess()))
				.match(MapTilesProtocol.SucceedResponse.class, message ->
						this.handleSucceedResponse(message.getProcess()))
				.match(MapTilesProtocol.FailResponse.class, message ->
						this.handleFailResponse(message.getProcess(), message.getMessage()))
				.match(MapTilesProtocol.CancelResponse.class, message ->
						this.handleCancelResponse(message.getProcess(), message.getMessage()))
				.match(MapTilesProtocol.CleanRequest.class, message ->
						this.handleCleanRequest())
				.match(MapTilesProtocol.ProcessesRequest.class, message -> this.handleProcessesRequest())
				.match(MapTilesProtocol.TilesFoldersRequest.class, message ->
						this.handleTilesFoldersRequest())
				.build();
	}

	private void handleFileChange(File file, String eventName, MapTilesProtocol.FileType fileType){
		switch (eventName) {
			case "ENTRY_MODIFY":
				return;
				//break;
			case "ENTRY_CREATE":
				logger.warn("ENTRY_CREATE {}", file.getAbsolutePath());
				break;
			case "ENTRY_DELETE":
				return;
		}

		MapTilesProcess process = MapTilesProcess.enQueue(file.getAbsolutePath(), fileType);
		processes.add(process);
		processorActor.tell(new MapTilesProtocol.RunRequest(process), this.getSelf());
	}

	private void handleRunResponse(MapTilesProcess process){
		logger.debug("Run " + process.getId() + " " + process.getFile());
		this.ensureTilesFolderExists();
		process.run();
	}

	private void handleSucceedResponse(MapTilesProcess process){
		logger.debug("Succeed " + process.getId() + " " + process.getFile());
		process.succeed();
		// The list of tiles folders is now updated on user request
		//updateTilesFolders();
		//
	}

	private void handleFailResponse(MapTilesProcess process, String message) {
		logger.debug("fail " + process.getId() + " " + process.getFile() + " " + message);
		process.fail(message);
	}

	private void handleCancelResponse(MapTilesProcess process, String message) {
		logger.debug("cancel " + process.getId() + " " + process.getFile() + " " + message);
		process.fail(message);
	}

	private void handleCleanRequest(){
		this.processes = processes.stream()
				.filter(process -> (process.getStatus() == MapTilesProcess.Status.QUEUED
						|| process.getStatus() == MapTilesProcess.Status.RUNNING))
				.collect(Collectors.toList());
		logger.trace("cleaned, now got %d active processes {}", processes.size());
	}

	private void handleProcessesRequest(){
		sender().tell(new MapTilesProtocol.ProcessesResponse(this.processes), this.getSelf());
	}

	private void handleTilesFoldersRequest(){
		// The list of tiles folders is now updated on user request
		this.updateTilesFolders();
		//
		sender().tell(new MapTilesProtocol.TilesFoldersResponse(this.tilesFolders), this.getSelf());
	}

	private void updateTilesFolders(){
		this.tilesFolders.clear();
		File[] files = new File(BuiltInPaths.FOLDER_TILES).listFiles();
		if ( files == null )
			return;
		for (File file : files) {
			if (file.isDirectory()) {
				//logger.trace("Tiles Folder found: " + file.getName());
				tilesFolders.add(file.getName());
			}
		}
	}
}

