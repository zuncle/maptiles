package fr.pixime.maptiles.actors;

import java.io.File;
import java.util.List;

public class MapTilesProtocol {

	public enum FileType {
		ZIP,
		SHAPEFILE,
		JSON,
		MBTILES
	}

	public static class HandleFileChange {
		private final File file;
		private final String eventName;
		private final FileType fileType;

		public HandleFileChange(File file, String eventName, FileType fileType) {
			this.file = file;
			this.eventName = eventName;
			this.fileType = fileType;
		}

		public File getFile() {
			return file;
		}

		public String getEventName() {
			return eventName;
		}

		public FileType getFileType(){
			return this.fileType;
		}
	}

	public static class ProcessMessage {
		private final MapTilesProcess process;

		public ProcessMessage(MapTilesProcess process) {
			this.process = process;
		}

		public MapTilesProcess getProcess() {
			return process;
		}
	}

	public static class RunRequest extends ProcessMessage{
		public RunRequest(MapTilesProcess process) {
			super(process);
		}
	}

	public static class RunResponse extends ProcessMessage{
		public RunResponse(MapTilesProcess process) {
			super(process);
		}
	}

	public static class SucceedResponse extends ProcessMessage {
		public SucceedResponse(MapTilesProcess process) {
			super(process);
		}
	}

	public static class FailResponse extends ProcessMessage {
		private final String message;
		public FailResponse(MapTilesProcess process, String message) {
			super(process);
			this.message = message;
		}
		public String getMessage() {
			return message;
		}
	}

	public static class CancelResponse extends ProcessMessage {
		private final String message;
		public CancelResponse(MapTilesProcess process, String message) {
			super(process);
			this.message = message;
		}
		public String getMessage() {
			return message;
		}
	}

	public static class CleanRequest {
		public CleanRequest() {
		}
	}

	public static class ProcessesRequest {
		public ProcessesRequest() {
		}
	}

	public static class ProcessesResponse {
		private final List<MapTilesProcess> processes;

		public ProcessesResponse(List<MapTilesProcess> processes) {
			this.processes = processes;
		}

		public List<MapTilesProcess> getProcesses() {
			return processes;
		}
	}

	public static class TilesFoldersRequest {
		public TilesFoldersRequest() {
		}
	}

	public static class TilesFoldersResponse {
		private final List<String> tilesFolders;

		public TilesFoldersResponse(List<String> tilesFolders) {
			this.tilesFolders = tilesFolders;
		}

		public List<String> getTilesFolders() {
			return tilesFolders;
		}
	}

}
