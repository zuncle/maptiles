package fr.pixime.maptiles.actors;

import akka.actor.AbstractActor;
import fr.pixime.maptiles.models.BuiltInPaths;
import fr.pixime.maptiles.utils.Json2MbTiles.Json2MbTiles;
import fr.pixime.maptiles.utils.Mbtiles2Tiles.Mbtiles2Tiles;
import fr.pixime.maptiles.utils.Shp2Json.Shp2Json;
import fr.pixime.maptiles.utils.Zip2Shp;
import org.apache.commons.io.FilenameUtils;
import play.Logger;

import javax.inject.Inject;

import java.io.File;

import static play.Logger.of;

public class ProcessorActor extends AbstractActor {

	private static final Logger.ALogger logger = of(ProcessorActor.class);

	@Inject
	public ProcessorActor() {
		logger.debug("[ProcessorActor] constructor");
	}

	@Override
	public void preStart() throws Exception {
		logger.info("[ProcessorActor] preStart");
		super.preStart();
	}

	@Override
	public void postStop() throws Exception {
		logger.info("[ProcessorActor] postStop");
		super.postStop();
	}

	private void onExecCommandOutput(MapTilesProcess process, String output) {
		// TODO To be pushed in websocket
		logger.trace(output);
	}

	private void onExecCommandError(MapTilesProcess process, String error) {
		logger.trace(error);
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
			.match(MapTilesProtocol.RunRequest.class, message ->
					this.process(message.getProcess()))
			.build();
	}

	public static String toValidFileName(String input){
		return input.replaceAll("[:\\\\/*\"?|<>']", "").toLowerCase();
	}

	private MapTilesProcess.Status processZip(MapTilesProcess process){
		//return Zip2Shp.generate(new File(process.getFile())) ?
		//		MapTilesProcess.Status.SUCCESS: MapTilesProcess.Status.FAILED;
		String shapefileName = Zip2Shp.generate(new File(process.getFile()));
		if ( shapefileName == null )
			return MapTilesProcess.Status.FAILED;

		String folder = FilenameUtils.getFullPath(process.getFile());
		String shapefilePath = String.format("%s%s", folder, shapefileName);
		Shp2Json shp2Json = new Shp2Json(shapefilePath);
		return shp2Json.generate(process);
	}

	private MapTilesProcess.Status processJson(MapTilesProcess process){
		Json2MbTiles json2MbTiles = new Json2MbTiles(process.getFile());
		return json2MbTiles.generate(process);
	}

	/*
	private MapTilesProcess.Status processShapefile(MapTilesProcess process) {
		Shp2Json shp2Json = new Shp2Json(process.getFile());
		return shp2Json.generate(process);
	}
	 */

	private MapTilesProcess.Status processMbTiles(MapTilesProcess process) {
		String tilesPath = toValidFileName(FilenameUtils.getBaseName(process.getFile()));
		Mbtiles2Tiles converter = new Mbtiles2Tiles(
				new File(process.getFile()).getAbsolutePath(),
				BuiltInPaths.FOLDER_TILES + tilesPath);

		return converter.generate(process);
	}

	private void process(MapTilesProcess process){
		process.setOnOutput(this::onExecCommandOutput);
		process.setOnError(this::onExecCommandError);

		sender().tell(
				new MapTilesProtocol.RunResponse(
						process),
				this.getSelf());

		MapTilesProcess.Status result = MapTilesProcess.Status.CANCELLED;
		switch (process.getFileType()){
			case ZIP:
				result = processZip(process);
				break;
			//case SHAPEFILE:
			//	result = processShapefile(process);
			//	break;
			case JSON:
				result = processJson(process);
				break;
			case MBTILES:
				result = processMbTiles(process);
				break;
		}

		switch (result){
			case SUCCESS:
				sender().tell(
						new MapTilesProtocol.SucceedResponse(
								process),
						this.getSelf());
				break;
			case FAILED:
				sender().tell(
						new MapTilesProtocol.FailResponse(
								process,
								"Errored"),
						this.getSelf());
				break;
			case CANCELLED:
				sender().tell(
						new MapTilesProtocol.CancelResponse(
								process,
								"Cancelled"),
						this.getSelf());
				break;
		}
	}
}