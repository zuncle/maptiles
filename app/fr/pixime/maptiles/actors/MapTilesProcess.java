package fr.pixime.maptiles.actors;

import fr.pixime.maptiles.utils.IThreadExecCommandFunc;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

public class MapTilesProcess {

	public enum Status {
		QUEUED,
		RUNNING,
		SUCCESS,
		FAILED,
		CANCELLED,
	}

	private String id;
	private String file;
	private String message;
	private Status status;
	private LocalDateTime queuedDate;
	private LocalDateTime runDate;
	private LocalDateTime endDate;
	private MapTilesProtocol.FileType fileType;

	private IThreadExecCommandFunc onOutput;
	private IThreadExecCommandFunc onError;

	private MapTilesProcess(){
		this.id = UUID.randomUUID().toString();
	}

	public static MapTilesProcess enQueue(String file,
										  MapTilesProtocol.FileType fileType){
		MapTilesProcess process = new MapTilesProcess();
		process.file = file;
		process.fileType = fileType;
		process.status = Status.QUEUED;
		process.queuedDate = LocalDateTime.now();
		process.message = "Waiting...";
		return process;
	}

	public void run(){
		this.status = Status.RUNNING;
		this.runDate = LocalDateTime.now(ZoneOffset.UTC);
		this.message = "Processing...";
	}

	public void succeed(){
		this.status = Status.SUCCESS;
		this.endDate = LocalDateTime.now(ZoneOffset.UTC);
		this.message = "Processed";
	}

	public void fail(String message){
		this.status = Status.FAILED;
		this.endDate = LocalDateTime.now(ZoneOffset.UTC);
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public String getFile() {
		return file;
	}

	public String getMessage() {
		return message;
	}

	public Status getStatus() {
		return status;
	}

	public LocalDateTime getQueuedDate() {
		return queuedDate;
	}

	public LocalDateTime getRunDate() {
		return runDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public MapTilesProtocol.FileType getFileType() {
		return fileType;
	}

	public IThreadExecCommandFunc getOnOutput() {
		return onOutput;
	}

	public void setOnOutput(IThreadExecCommandFunc onOutput) {
		this.onOutput = onOutput;
	}

	public IThreadExecCommandFunc getOnError() {
		return onError;
	}

	public void setOnError(IThreadExecCommandFunc onError) {
		this.onError = onError;
	}
}
