package fr.pixime.maptiles.filters;

import akka.stream.Materializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Filter;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class AccessControlAllowOriginFilter extends Filter {
    private static final Logger log = LoggerFactory.getLogger(AccessControlAllowOriginFilter.class);

    @Inject
    public AccessControlAllowOriginFilter(Materializer mat) {
        super(mat);
    }

    @Override
    public CompletionStage<Result> apply(
            Function<Http.RequestHeader, CompletionStage<Result>> nextFilter,
            Http.RequestHeader requestHeader) {
        return nextFilter.apply(requestHeader).thenApply(result -> {
            return result.withHeader("Access-Control-Allow-Origin", "*");
        });
    }
}
